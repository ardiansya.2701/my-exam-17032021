package project;

import java.util.ArrayList;
import java.util.List;

public class Exam {

	/*
	1. We have array of integers called nums, write a function to return all numbers (in a form
	of array of integers) that when subtracted by any of integers in nums doesn't return
	number that is < 0
	for example : nums = [3,1,4,2], your output should be : [4], because when 4 is subtracted
	by 3 or 1 or 4 or 2 doesn't return number that is < 0
	
	2. We have array of integers called nums and an integer called x, write a function to return
	all numbers (in a form of array of integers) that when divided by any of integers in nums
	doesn't return x
	for example : nums = [1,2,3,4], x = 4, your output should be : [1,2,3], because only 4
	divided by 1 is 4 (x)
	
	3. We have a string called word and an integer called x, write a function to return an array
	of strings containing all strings that has length x.
	for example : word = "souvenir loud four lost", x = 4, your output should be ["loud", "four",
	"lost"] because those strings has length of 4
	*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums1 = {3,1,4,2};
		int[] nums2 = {1,2,3,4};
		String word = "souvenir loud four lost";
		int x2 = 4;
		int x3 = 4;
		Exam test = new Exam();
		
		List<Integer> test1 = test.funcSubtracted(nums1);
		System.out.println("result 1 : "+test1);
		
		List<Integer> test2 = test.funcDivided(nums2, x2);
		System.out.println("result 2 : "+test2);
		
		List<String> test3  = test.funcWords(word, x3);
		System.out.println("result 3 : "+test3);
	}
	
	public List<Integer> funcSubtracted(int[] nums) {
		List<Integer> arrInt = new ArrayList<>();
		
		try {
			int tempNums = -1;
			for (int i = 0; i < nums.length; i++) {
				for (int j = 0; j < nums.length; j++) {
					//System.out.println(nums[i] - nums[j]);
					if (nums[i] - nums[j] < 0) {
						tempNums = -1;
						break;
					} else {
						tempNums = nums[i];
					}
				}
				//System.out.println("ii "+i+"tempNums");
				if (tempNums != -1) {
					arrInt.add(tempNums);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arrInt;
	}
	
	public List<Integer> funcDivided(int[] nums, int x) {
		List<Integer> arrInt = new ArrayList<>();
		try {
			for (int i = 0; i < nums.length; i++) {
				//System.out.println(nums[i]/x);
				if (nums[i] / x != 1) {
					arrInt.add(nums[i]);
				} 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arrInt;
	}

	public List<String> funcWords(String word, int x) {
		List<String> arrStr = new ArrayList<>();
		try {
			String[] arrWord = word.split(" ");
			for (int i = 0; i < arrWord.length; i++) {
				if (arrWord[i].length() == x) {
					arrStr.add(arrWord[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arrStr;
	}
	
}
